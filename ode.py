import numpy as np


def solve_linear(A, b):
	A = np.array(A)
	b = np.array(b).reshape(len(b), 1)
	M = np.hstack((A, b))
	for i in range(M.shape[0]):
		max_elem_row = sorted(zip(M[i:, i], np.arange(i, M.shape[0])), key=lambda x: np.fabs(x[0]))[-1][1]
		#max_elem_row = i
		tmp = np.array(M[i])
		M[i] = M[max_elem_row]
		M[max_elem_row] = tmp
		
		if M[i, i] == 0:
			return None
		M[i] =  M[i] / M[i, i]
		
		for j in range(M.shape[0]):
			if j != i:
				M[j] = M[j] - M[i] * M[j][i]

	return M[:,-1]


def solve_cauchy(x0, y0, tn, g, f):
	print ("solve_cauchy called")
	n = len(tn)
	xn = np.zeros(n)
	xn[0] = x0
	yn = np.zeros(n)
	yn[0] = y0
	for i in range(n-1):
		xi = xn[i]
		yi = yn[i]
		ti = tn[i]
		hi = tn[i+1] - tn[i]
		kx0 = hi*g(ti, xi, yi)
		ky0 = hi*f(ti, xi, yi)

		#xn[i+1] = xi + hi*kx0
		#yn[i+1] = yi + hi*ky0
		
		ky1 = hi*g(ti + hi/2, xi + kx0/2, yi + ky0/2)
		kx1 = hi*f(ti + hi/2, xi + kx0/2, yi + ky0/2)
		
		kx2 = hi*g(ti + hi/2, xi + kx1/2, yi + ky1/2)
		ky2 = hi*f(ti + hi/2, xi + kx1/2, yi + ky1/2)
		
		kx3 = hi*g(ti + hi, xi + kx2, yi + ky2)
		ky3 = hi*f(ti + hi, xi + kx2, yi + ky2)

		xn[i+1] = xi + (kx0 + 2*kx1 + 2*kx2 + kx3)/6
		yn[i+1] = yi + (ky0 + 2*ky1 + 2*ky2 + ky3)/6
	
	return xn, yn
