import numpy as np


def differentiate(splines):
	return np.array([[spline[1], 2*spline[2], 3*spline[3], 0, spline[4]] for spline in splines])