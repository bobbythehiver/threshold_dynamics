# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
from matplotlib import rc
import numpy as np
from numpy import e, pi, exp, log, sin, cos, tan
import scipy as sc
import datetime

from interpolation import tabulate, get_points
from interpolation import interpolate, interpolate_tab, recover
from integration import integrate
from differentiation import differentiate
from cauchy import solve_cauchy, solve_linear

from funcio import read_splines_from_file, write_splines_to_file
from funcio import read_func_from_str, read_f
from funcio import read_tabulated_func_from_file, write_tabulated_func_to_file

font = {'family': 'Verdana',
        'weight': 'normal',
        'size': 12}
rc('font', **font)


UNI=0
CHEB=1

N=20

Y_UNI = get_points(0, 1, N, mode=UNI)
Y_CHEB = get_points(0, 1, N, mode=CHEB)


def compute_c1(x, y, g, wp_integrate, x0, T):
	print ("compute_c1 called")

	tn = get_points(0, T, N, mode=UNI)

	func = lambda t: g(t, x(t), y(t)) * (1-wp_integrate(y(t)))
	c1 = 1 - 1/(x(T) - x0)*integrate(func, tn)[-1]
	return c1


def compute_c2(x, s, T):
	print ("compute_c2 called")
	c2 = np.fabs(x(T) - s(T)) / s(T)
	return c2


def compute_quality_func(c1, c2):
	print ("compute_quality_func called")
	return c1 + 10*c2


def visualize21(xn1, xlabel1, yn1, ylabel1, title1,
	xn2, xlabel2, yn2, ylabel2, title2):
	plt.figure()
	plt.subplot(211)
	plt.plot(xn1, yn1, 'r')
	plt.xlabel(xlabel1)
	plt.ylabel(ylabel1)
	plt.title(title1)
	plt.grid()

	plt.subplot(212)
	plt.plot(xn2, yn2, 'r')
	plt.xlabel(xlabel2)
	plt.ylabel(ylabel2)
	plt.title(title2)
	plt.grid()

	plt.tight_layout()


def visualize_txs(tn, xn, sn, title=''):
	fig = plt.figure()
	fig.suptitle(title, fontsize=14)
	fig.subplots_adjust(top=1.5)
	s1 = fig.add_subplot(221)
	s1.plot(tn, xn, 'r')
	s1.set_xlabel('t')
	s1.set_ylabel('x')
	s1.set_title('\nx(t)')
	s1.grid()

	s2 = fig.add_subplot(222)
	sline, = s2.plot(tn, sn, 'r')
	xline, = s2.plot(tn, xn, 'b')
	s2.legend([sline, xline], ['S(t)', 'x(t)'], fontsize=10, loc="best")
	s2.set_xlabel('t')
	s2.set_ylabel('S')
	s2.set_title('\nS(t)')
	s2.grid()

	s3 = fig.add_subplot(223)
	s3.plot(tn, xn-sn, 'r')
	s3.set_xlabel('t')
	s3.set_ylabel('x - S')
	s3.set_title('x(t) - S(t)')
	s3.grid()

	s4 = fig.add_subplot(224)
	s4.plot(xn, sn, 'r')
	s4.set_xlabel('x')
	s4.set_ylabel('S')
	s4.set_title('S(x)')
	s4.grid()

	fig.tight_layout()


def create_filename(str_id):
	return str_id + "_tmp.txt"
	#return datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S") + str_id + '.txt'


def compute_manual(p_str, s_str, f_str, z_str, b, x0, y0, T):
	print ("compute_manual called")

	p = lambda w: None
	s = lambda t: None
	z = lambda t: None

	bad_string = ''

	tn = get_points(0, T, N, mode=UNI)

	file_p, p = read_tabulated_func_from_file(p_str)
	if not file_p:
		entry_p, p = read_func_from_str(p_str, arg='W')
		if not entry_p:
			bad_string = bad_string + 'p'

	file_z, z = read_tabulated_func_from_file(z_str)
	if not file_z:
		entry_z, z = read_func_from_str(z_str, arg='T')
		if not entry_z:
			bad_string = bad_string + 'z'
	
	file_s, s = read_tabulated_func_from_file(s_str)
	if not file_s:
		entry_s, s = read_func_from_str(s_str, arg='T')
		if not entry_s:
			bad_string = bad_string + 's'

	if bad_string != '':
		return bad_string + 'f', None, None

	entry_f, f = read_f(f_str, s, z, x0, b)
	if not entry_f:
		bad_string = bad_string + 'f'

	if bad_string != '':
		return bad_string, None, None

	f_b = lambda t, x, y: f(t, x, y, b)


	z_prime_filename = create_filename('z')
	z_splines = interpolate(z, tn)
	z_prime_splines = differentiate(z_splines)
	write_splines_to_file(z_prime_splines, z_prime_filename)
	z_prime = recover(read_splines_from_file(z_prime_filename))

	u_filename = create_filename('u')
	u_splines = interpolate_tab(Y_UNI, integrate(p, Y_UNI))
	write_splines_to_file(u_splines, u_filename)
	u = recover(read_splines_from_file(u_filename))

	g = lambda t, x, y: (1 - u(y))*z_prime(t)

	xn, yn = solve_cauchy(x0, y0, tn, g, f_b)

	x_filename = create_filename('x')
	x_splines = interpolate_tab(tn, xn)
	write_splines_to_file(x_splines, x_filename)
	x = recover(read_splines_from_file(x_filename))

	y_filename = create_filename('y')
	y_splines = interpolate_tab(tn, yn)
	write_splines_to_file(y_splines, y_filename)
	y = recover(read_splines_from_file(y_filename))

	wp = lambda w: w*p(w)
	wp_integrate_filename = create_filename('wp')
	wp_splines = interpolate_tab(Y_UNI, integrate(wp, Y_UNI))
	write_splines_to_file(wp_splines, wp_integrate_filename)
	wp_integrate = recover(read_splines_from_file(wp_integrate_filename))
	
	c1, c2 = compute_c1(x, y, g, wp_integrate, x0, T), compute_c2(x, s, T)

	sn = tabulate(s, tn)
	zn = tabulate(z, tn)
	wn = np.arange(0, 1, 0.01)
	pn = tabulate(p, wn)

	visualize21(wn, 'w', pn, 'p', u'Плотность p(w)', tn, 't', zn, 'z', u'Функция трафика z(t)')
	visualize21(tn, 't', xn, 'x', u'Решение задачи Коши: \nx(t)', tn, 't', yn, 'y', u'y(t)')
	visualize_txs(tn, xn, sn)
	plt.show()

	return '', c1, c2


def beta_search(wp_integrate, s, f, g, b_from, b_to, y0, T):
	print("beta_search called")
	
	tn = get_points(0, T, N, mode=UNI)
	b = get_points(b_from, b_to, 11)

	phi_min = 1e9
	b_min = b_to
	for bi in b:
		f_b = lambda t, x, y: f(t, x, y, bi)
		
		xn, yn = solve_cauchy(s(0), y0, tn, g, f_b)
		
		sn = tabulate(s, tn)

		x_filename = create_filename('x')
		x_splines = interpolate_tab(tn, xn)
		write_splines_to_file(x_splines, x_filename)
		x = recover(read_splines_from_file(x_filename))

		y_filename = create_filename('y')
		y_splines = interpolate_tab(tn, yn)
		write_splines_to_file(y_splines, y_filename)
		y = recover(read_splines_from_file(y_filename))
		
		c1, c2 = compute_c1(x, y, g, wp_integrate, s(0), T), compute_c2(x, s, T)
		phi = compute_quality_func(c1, c2)
		if phi < phi_min:
			b_min = bi
	
	return b_min


def compute_auto(p_str, s_str, f_str, z_str, b_from, b_to, y0, T):
	print ("compute_auto called")
	p = lambda w: None
	s = lambda t: None
	z = lambda t: None

	bad_string = ''

	tn = get_points(0, T, N, mode=UNI)

	file_p, p = read_tabulated_func_from_file(p_str)
	if not file_p:
		entry_p, p = read_func_from_str(p_str, arg='W')
		if not entry_p:
			bad_string = bad_string + 'p'

	file_z, z = read_tabulated_func_from_file(z_str)
	if not file_z:
		entry_z, z = read_func_from_str(z_str, arg='T')
		if not entry_z:
			bad_string = bad_string + 'z'
	
	file_s, s = read_tabulated_func_from_file(s_str)
	if not file_s:
		entry_s, s = read_func_from_str(s_str, arg='T')
		if not entry_s:
			bad_string = bad_string + 's'
	
	if bad_string != '':
		return bad_string +'f', None, None

	entry_f, f = read_f(f_str, s, z, s(0), b_from)
	if not entry_f:
		bad_string = bad_string + 'f'

	if bad_string != '':
		return bad_string, None, None

	z_prime_filename = create_filename('z')
	z_splines = interpolate(z, tn)
	z_prime_splines = differentiate(z_splines)
	write_splines_to_file(z_prime_splines, z_prime_filename)
	z_prime = recover(read_splines_from_file(z_prime_filename))
 
	u_filename = create_filename('u')
	u_splines = interpolate_tab(Y_UNI, integrate(p, Y_UNI))
	write_splines_to_file(u_splines, u_filename)
	u = recover(read_splines_from_file(u_filename))

	g = lambda t, x, y: (1 - u(y))*z_prime(t)

	wp = lambda w: w*p(w)
	wp_integrate_filename = create_filename('wp')
	wp_splines = interpolate_tab(Y_UNI, integrate(wp, Y_UNI))
	write_splines_to_file(wp_splines, wp_integrate_filename)
	wp_integrate = recover(read_splines_from_file(wp_integrate_filename))

	b = beta_search(wp_integrate, s, f, g, b_from, b_to, y0, T)
	f_b = lambda t, x, y: f(t, x, y, b)
	
	y0n = get_points(0, 1, 5)

	c1 = 0
	c2 = 0

	pointsx0 = []
	pointsy0 = []
	points = []

	zn = tabulate(z, tn)
	wn = get_points(0, 1, 100)
	pn = tabulate(p, wn)
	visualize21(wn, 'w', pn, 'p', u'Плотность p(w)', tn, 't', zn, 'z', u'Функция трафика z(t)')
	plt.show()
	
	for y0 in y0n:
		xn, yn = solve_cauchy(x0=s(0), y0=y0, tn=tn, g=g, f=f_b)

		x_filename = create_filename('x')
		x_splines = interpolate_tab(tn, xn)
		write_splines_to_file(x_splines, x_filename)
		x = recover(read_splines_from_file(x_filename))

		y_filename = create_filename('y')
		y_splines = interpolate_tab(tn, yn)
		write_splines_to_file(y_splines, y_filename)
		y = recover(read_splines_from_file(y_filename))

		c1, c2 = compute_c1(x, y, g, wp_integrate, xn[0], T), compute_c2(x, s, T)

		sn = tabulate(s, tn)

		visualize21(tn, 't', xn, 'x', 
			u'Решение задачи Коши\nc начальными условиями x0 = %.2f, y0 = %.2f: \nx(t)' % (x(0), y(0)), 
			tn, 't', yn, 'y', u'y(t)')
		visualize_txs(tn, xn, sn, u'Начальные условия задачи Коши: x0 = %.2f, y0 = %.2f' % (x(0), y(0)))
		plt.show()

	print c1, c2
	return '', c1, c2
