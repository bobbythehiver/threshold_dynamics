import numpy as np


UNI=0
CHEB=1

def cheb_transform(x, a, b):
	return float(2*x - a - b) / (b - a)


def inv_cheb_transform(x, a, b):
	return float(x*(b-a) + a + b)/2


def get_points(a, b, n, mode=UNI):
	if a == b:
		return [a]
	if mode==CHEB:
		cheb_tab_points = [np.cos(float(2.0*i+1)/(2.0*n)*np.pi) for i in range(n)]
		tab_points = sorted([inv_cheb_transform(point, a, b) for point in cheb_tab_points])
		return tab_points
	else:
		h = float(b-a)/(n-1)
		tab_points = np.arange(a, b + h/2.0, h)
		return tab_points


def tabulate(f, points):
	y = [f(point) for point in points]
	return y


def spline_value(splines, x):
	xn = [spline[4] for spline in splines]
	n = len(xn)
	idx = max(0, min(n-1, np.searchsorted(xn, x)))
	dx = x - xn[idx]
	
	return splines[idx][0] + splines[idx][1]*dx + splines[idx][2]*dx**2/2.0 + splines[idx][3]*dx**3/6.0


def recover(splines):
	print ("recover called")
	return lambda x: spline_value(splines, x)


def interpolate_tab(xn, yn):
	print ("interpolate_tab called")
	a, b, c, d, x = 0, 1, 2, 3, 4 
	n = len(xn)
	splines = np.zeros((n, 5))
	for i in range(n): 
		splines[i][x] = xn[i]
		splines[i][a] = yn[i]

	splines[0][c] = 0.0
	splines[-1][c] = 0.0

	alpha = np.zeros(n-1)
	beta = np.zeros(n-1)

	for i in range(1, n-1):
		hi_left = xn[i] - xn[i-1]
		hi_right = xn[i+1] - xn[i]

		C = 2.0*(hi_left + hi_right)
		F = 6.0*((yn[i+1] - yn[i]) / hi_right - (yn[i] - yn[i-1]) / hi_left)
		z = (hi_left * alpha[i-1] + C)
		alpha[i] = -hi_right / z
		beta[i] = (F - hi_left * beta[i-1]) / z

	for i in reversed(range(1, n-1)): 
		splines[i][c] = alpha[i] * splines[i+1][c] + beta[i]

	for i in reversed(range(1, n)):
		hi = xn[i] - xn[i-1]
		splines[i][d] = (splines[i][c] - splines[i-1][c]) / hi
		splines[i][b] = hi * (2.0 * splines[i][c] + splines[i-1][c]) / 6.0 + (yn[i] - yn[i-1]) / hi

	return splines


def interpolate(f, xn):
	print ("interpolate called")
	yn = tabulate(f, xn)
	return interpolate_tab(xn, yn)