import numpy as np


def integrate(f, tn):
	print ("integrate called")
	n = len(tn)
	integral = np.zeros(n)
	for i in range(n-1):
		integral[i+1] = integral[i] + (tn[i+1] - tn[i])/6.0*(f(tn[i]) + 4*f((tn[i] + tn[i+1])/2.0) + f(tn[i+1]))

	return integral