import numpy as np
from numpy import e, pi, exp, log, sin, cos, tan

from interpolation import recover, interpolate, interpolate_tab


def read_func_from_str(func_str, arg=''):
	try:
		func = lambda x: float(eval(func_str.replace(arg, '('+str(x)+')')))
		func(0)
		return True, func
	except:
		func = lambda x: None
		return False, func


def read_f(f_str, s, z, x0, b):
	f = lambda t, x, y, b: float(eval(f_str.replace('T', '('+str(t)+')')
		.replace('X', '('+str(x)+')')
		.replace('B', '('+str(b)+')')
		.replace('Z', '('+str(z(t))+')')
		.replace('S', '('+str(s(t))+')')))
	f(0, x0, 0, b)
	return True, f


def read_splines_from_file(filename):
	f = open(filename, 'r')
	splines = np.array(map(float, f.read().strip().split(' ')))
	f.close()
	return splines.reshape((len(splines) // 5, 5))


def write_splines_to_file(splines, filename):
	f = open(filename, 'w')
	f.write(' '.join(map(str, splines.reshape(len(splines)*5, ))))
	f.close()
	return


def read_tabulated_func_from_file(filename):
	try:
		f = open(filename, 'r')
		contains = list(f.read().strip().split(' '))
		f.close()
		n = int(contains[0])
		contains = [float(x) for x in contains[1:]]

		xn = contains[0:n]
		yn = contains[n:]

		return True, recover(interpolate_tab(xn, yn))
	except:
		return False, None


def write_tabulated_func_to_file(xn, yn, filename):
	f = open(filename, 'w')
	f.write(str(len(xn)) + ' ')
	f.write(' '.join(map(str, xn)))
	f.write(' ')
	f.write(' '.join(map(str, yn)))
	f.close()
	return
