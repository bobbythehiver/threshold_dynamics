# -*- coding: utf-8 -*-
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure
import Tkinter as tk
from Tkinter import END
import ttk

from solve import compute_manual, compute_auto
from validator import validate_float

BGCOLOR = '#363636' #'#1C1C1C' #'#00CDCD'
FGCOLOR = '#CFCFCF'
ACTIVEBGCOLOR = '#CFCFCF'
ACTIVEFGCOLOR = '#000000'
VALIDCOLOR = 'red'

FONT = ("Century Gothic", 12, "normal")
LARGEFONT = ("Century Gothic", 14, "normal")
SMALLFONT = ("Century Gothic", 10, "normal")
VALIDFONT = ("Century Gothic", 8, "normal")
TITLEFONT = ("Century Gothic", 16, "normal")

class Demo(tk.Tk):
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)

        #tk.Tk.iconbitmap(self, default="clienticon.ico")

        tk.Tk.wm_title(self, "Demo")
        self.configure()

        mainframe = tk.Frame(self)
        #mainframe.pack(side="top", fill="both", expand = True)
        mainframe.grid_rowconfigure(0, weight=1)
        mainframe.grid_columnconfigure(0, weight=1)
        mainframe.grid()

        self.frames = {}

        for F in (StartPage, ManualMode, AutoMode):

            frame = F(mainframe, self)

            self.frames[F] = frame

            frame.configure(background=BGCOLOR)
            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame(StartPage)

    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()

        
class StartPage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self,parent)

        self.mode_ = "manual"
        
        label = ttk.Label(self, text=u"Главная", font=TITLEFONT, background=BGCOLOR, foreground=FGCOLOR)
        label.grid(row=0, column=0, columnspan=2, padx=10, pady=20)
        manual_mode_button = tk.Button(self, text=u"Ручной режим", font=LARGEFONT,
                                        background=BGCOLOR, foreground=FGCOLOR, 
                                        width=40, relief="flat",
                                        activebackground=ACTIVEBGCOLOR, activeforeground=ACTIVEFGCOLOR,
                                        command=lambda: controller.show_frame(ManualMode))
        manual_mode_button.grid(row=1, column=0, padx=10, pady=200)
        auto_mode_button = tk.Button(self, text=u"Автоматический режим", font=LARGEFONT, 
                                        background=BGCOLOR, foreground=FGCOLOR, 
                                        width=40, relief="flat",
                                        activebackground=ACTIVEBGCOLOR, activeforeground=ACTIVEFGCOLOR,
                                        command=lambda: controller.show_frame(AutoMode))
        auto_mode_button.grid(row=1, column=1, padx=10, pady=200)


class ManualMode(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.title_label = ttk.Label(self, text=u"Ручной режим", font=TITLEFONT, 
                                    background=BGCOLOR, foreground=FGCOLOR)
        self.title_label.grid(row=0, column=0, columnspan=6, pady=20, padx=0)


        self.info_title_label = ttk.Label(self, text=u"Формат ввода", font=LARGEFONT,
                                    width=30, anchor='w',
                                    background=BGCOLOR, foreground=FGCOLOR)
        self.info_title_label.grid(row=2, column=5, pady=0, padx=10)

        self.info_text = tk.Text(self, font=SMALLFONT,
                                    width=50, relief='flat',
                                    background=BGCOLOR, foreground=FGCOLOR)
        self.info_text.grid(row=3, rowspan=10, column=5, pady=10, padx=50)
        self.info_text.insert(tk.INSERT, u"В вещественных числах целая и вещественная часть разделяются точкой.\nПараметры должны быть обозначены заглавными \nлатинскими буквами.\nОперация умножения задается знаком *,\nделения — /, возведения в степень — **.\nВозможно использование математических констант e, pi и функций sin, cos, tan, exp, log. \n\nФункции могут быть заданы неявно: в поле ввода \nукажите полный путь к файлу, где они хранятся в \nтабулированном виде в формате \nN N точек N значений, N>3")
        self.info_text.configure(state="disabled")

        self.show_example1_button = tk.Button(self, text=u"Показать пример №1", font=FONT,
                                        background=BGCOLOR, foreground=FGCOLOR, relief="flat",
                                        activebackground=ACTIVEBGCOLOR, activeforeground=ACTIVEFGCOLOR,
                                        command=self.show_example1)
        self.show_example1_button.grid(row=10, column=5, padx=0, pady=0)

        self.show_example2_button = tk.Button(self, text=u"Показать пример №2", font=FONT,
                                        background=BGCOLOR, foreground=FGCOLOR, relief="flat",
                                        activebackground=ACTIVEBGCOLOR, activeforeground=ACTIVEFGCOLOR,
                                        command=self.show_example2)
        self.show_example2_button.grid(row=11, column=5, padx=0, pady=0)

        self.show_example3_button = tk.Button(self, text=u"Показать пример №3", font=FONT,
                                        background=BGCOLOR, foreground=FGCOLOR, relief="flat",
                                        activebackground=ACTIVEBGCOLOR, activeforeground=ACTIVEFGCOLOR,
                                        command=self.show_example3)
        self.show_example3_button.grid(row=12, column=5, padx=0, pady=0)

        self.p_label = ttk.Label(self, text=u"Плотность распределения p(w) =", font=FONT, 
                                anchor='e', width=30, 
                                background=BGCOLOR, foreground=FGCOLOR)
        self.p_label.grid(row=1, column=0, padx=10, pady=0)
        self.p_entry = tk.Entry(self, width=30)
        self.p_entry.grid(row=1, column=1, columnspan=4, padx=0, pady=0, sticky='w')
        self.p_valid_label = ttk.Label(self, text=u"", font=VALIDFONT, 
                                        anchor='w', width=30,
                                        background=BGCOLOR, foreground=VALIDCOLOR)
        self.p_valid_label.grid(row=2, column=1, columnspan=4, padx=0, pady=0)

        
        self.s_label = ttk.Label(self, text=u"План открута S(t) =", font=FONT, 
                                anchor='e', width=30, 
                                background=BGCOLOR, foreground=FGCOLOR)
        self.s_label.grid(row=3, column=0, padx=10, pady=0)
        self.s_entry = tk.Entry(self, width=30)
        self.s_entry.grid(row=3, column=1, columnspan=4, padx=0, pady=0, sticky='w')
        self.s_valid_label = ttk.Label(self, text=u"", font=VALIDFONT, 
                                        anchor='w', width=30,
                                        background=BGCOLOR, foreground=VALIDCOLOR)
        self.s_valid_label.grid(row=4, column=1, columnspan=4, padx=0, pady=0)

        
        self.f_label = ttk.Label(self, text=u"Функция коррекции f(z, X, S, b) =", font=FONT,
                                anchor='e', width=30, 
                                background=BGCOLOR, foreground=FGCOLOR)
        self.f_label.grid(row=5, column=0, padx=10, pady=0)
        self.f_entry = tk.Entry(self, width=30)
        self.f_entry.grid(row=5, column=1, columnspan=4, padx=0, pady=0, sticky='w')
        self.f_valid_label = ttk.Label(self, text=u"", font=VALIDFONT, 
                                        anchor='w', width=30,
                                        background=BGCOLOR, foreground=VALIDCOLOR)
        self.f_valid_label.grid(row=6, column=1, columnspan=4, padx=0, pady=0)

         
        self.b_label = ttk.Label(self, text=u"b = ", font=FONT, 
                                anchor='e', width=30, 
                                background=BGCOLOR, foreground=FGCOLOR)
        self.b_label.grid(row=7, column=0, padx=10, pady=0)
        self.b_entry = tk.Entry(self, width=30)
        self.b_entry.grid(row=7, column=1, columnspan=4, padx=0, pady=0, sticky='w')
        self.b_valid_label = ttk.Label(self, text=u"", font=VALIDFONT, 
                                        anchor='w', width=30,
                                        background=BGCOLOR, foreground=VALIDCOLOR)
        self.b_valid_label.grid(row=8, column=1, columnspan=4, padx=0, pady=0)


        self.z_label = ttk.Label(self, text=u"Функция трафика z(t) =", font=FONT,
                                anchor='e', width=30, 
                                background=BGCOLOR, foreground=FGCOLOR)
        self.z_label.grid(row=9, column=0, padx=10, pady=0)
        self.z_entry = tk.Entry(self, width=30)
        self.z_entry.grid(row=9, column=1, columnspan=4, padx=0, pady=0, sticky='w')
        self.z_valid_label = ttk.Label(self, text=u"", font=VALIDFONT, 
                                        anchor='w', width=30,
                                        background=BGCOLOR, foreground=VALIDCOLOR)
        self.z_valid_label.grid(row=10, column=1, columnspan=4, padx=0, pady=0)

        
        self.xy_label = ttk.Label(self, text=u"Начальные условия:    X0 =", font=FONT,
                                anchor='e', width=30, 
                                background=BGCOLOR, foreground=FGCOLOR)
        self.xy_label.grid(row=11, column=0, padx=10, pady=0)

        self.x0_entry = tk.Entry(self, width=10)
        self.x0_entry.grid(row=11, column=1, padx=0, pady=0)
        self.x0_valid_label = ttk.Label(self, text=u"", font=VALIDFONT, 
                                        anchor='w', width=10,
                                        background=BGCOLOR, foreground=VALIDCOLOR)
        self.x0_valid_label.grid(row=12, column=1, padx=0, pady=0)
        
        self.y0_label = ttk.Label(self, text=u"Y0 =", font=FONT,
                                anchor='e', width=4, 
                                background=BGCOLOR, foreground=FGCOLOR)
        self.y0_label.grid(row=11, column=3, padx=0, pady=0)
        self.y0_entry = tk.Entry(self, width=10)
        self.y0_entry.grid(row=11, column=4, padx=0, pady=0)
        self.y0_valid_label = ttk.Label(self, text=u"", font=VALIDFONT, 
                                        anchor='w', width=10,
                                        background=BGCOLOR, foreground=VALIDCOLOR)
        self.y0_valid_label.grid(row=12, column=4, padx=0, pady=0)

        self.t_label = ttk.Label(self, text=u"T_final =", font=FONT,
                                anchor='e', width=30, 
                                background=BGCOLOR, foreground=FGCOLOR)
        self.t_label.grid(row=13, column=0, padx=10, pady=0)
        self.t_entry = tk.Entry(self, width=30)
        self.t_entry.grid(row=13, column=1, columnspan=4, padx=0, pady=0, sticky='w')
        self.t_valid_label = ttk.Label(self, text=u"", font=VALIDFONT, 
                                        anchor='w', width=30,
                                        background=BGCOLOR, foreground=VALIDCOLOR)
        self.t_valid_label.grid(row=14, column=1, columnspan=4, padx=0, pady=0)      


        self.compute_button = tk.Button(self, text=u"Рассчитать", font=LARGEFONT,
                                        background=BGCOLOR, foreground=FGCOLOR, relief="flat",
                                        activebackground=ACTIVEBGCOLOR, activeforeground=ACTIVEFGCOLOR,
                                        command=self.compute)
        self.compute_button.grid(row=15, column=0, columnspan=6, padx=0, pady=10)

        self.results_label = ttk.Label(self, text=u"", font=FONT, 
                                        background=BGCOLOR, foreground=FGCOLOR)
        self.results_label.grid(row=16, column=0, columnspan=6, pady=5)


        #canvas = FigureCanvasTkAgg(f, self)
        #canvas.show()
        #canvas.get_tk_widget().grid(row=8, column=2, columnspan=2, padx=0, pady=0)

        #toolbar = NavigationToolbar2TkAgg(canvas, self)
        #toolbar.update()
        #canvas._tkcanvas.grid(row=9, column=2, columnspan=2, padx=0, pady=0)

        self.return_button = tk.Button(self, text=u"Вернуться на главную", font=FONT,
                                        background=BGCOLOR, foreground=FGCOLOR, relief="flat",
                                        activebackground=ACTIVEBGCOLOR, activeforeground=ACTIVEFGCOLOR,
                                        command=lambda: controller.show_frame(StartPage))
        self.return_button.grid(row=17, column=0, columnspan=6)

        self.p_entry.insert(0, "6*W*(1-W)")
        self.s_entry.insert(0, "3*T + sin(T)")
        self.f_entry.insert(0, "B*(X-Z)")
        self.z_entry.insert(0, "4*T + cos(T)")
        self.b_entry.insert(0, "0.01")
        self.x0_entry.insert(0, "0.0")
        self.y0_entry.insert(0, "0.0")
        self.t_entry.insert(0, "1")


    def delete_entries(self):
        self.p_entry.delete(0, END)
        self.s_entry.delete(0, END)
        self.f_entry.delete(0, END)
        self.z_entry.delete(0, END)
        self.b_entry.delete(0, END)
        self.y0_entry.delete(0, END)
        self.t_entry.delete(0, END)


    def show_example1(self):
        self.delete_entries()
        self.p_entry.insert(0, "p.txt")
        self.s_entry.insert(0, "s.txt")
        self.f_entry.insert(0, "B*(S-X)")
        self.z_entry.insert(0, "z.txt")
        self.b_entry.insert(0, "0.01")
        self.y0_entry.insert(0, "0.0")
        self.t_entry.insert(0, "1")


    def show_example2(self):
        self.delete_entries()
        self.p_entry.insert(0, "1")
        self.s_entry.insert(0, "T")
        self.f_entry.insert(0, "B*(S-X)")
        self.z_entry.insert(0, "4*T")
        self.b_entry.insert(0, "1")
        self.y0_entry.insert(0, "0.0")
        self.t_entry.insert(0, "1")


    def show_example3(self):
        self.delete_entries()
        self.p_entry.insert(0, "2*W")
        self.s_entry.insert(0, "10*T*T")
        self.f_entry.insert(0, "1")
        self.z_entry.insert(0, "T*T")
        self.b_entry.insert(0, "0.0")
        self.y0_entry.insert(0, "0.0")
        self.t_entry.insert(0, "1")



    def compute(self):
        print("compute called")

        self.p_valid_label.configure(text=u"")
        self.s_valid_label.configure(text=u"")
        self.z_valid_label.configure(text=u"")
        self.f_valid_label.configure(text=u"")
        self.b_valid_label.configure(text=u"")
        self.x0_valid_label.configure(text=u"")
        self.y0_valid_label.configure(text=u"")
        self.t_valid_label.configure(text=u"")
        self.results_label.configure(text=u"")

        is_valid_b, b = validate_float(self.b_entry.get())
        if not is_valid_b:
            self.b_valid_label.configure(text=u"ошибка")
        else:
            self.b_valid_label.configure(text=u"")
        
        is_valid_x0, x0 = validate_float(self.x0_entry.get())
        if not is_valid_x0:
            self.x0_valid_label.configure(text=u"ошибка")
        else:
            self.x0_valid_label.configure(text=u"")
        
        is_valid_y0, y0 = validate_float(self.y0_entry.get())
        if is_valid_y0 and y0>1:
            is_valid_y0 = False
        if not is_valid_y0:
            self.y0_valid_label.configure(text=u"ошибка")
        else:
            self.y0_valid_label.configure(text=u"")

        is_valid_t, t = validate_float(self.t_entry.get())
        if not is_valid_t:
            self.t_valid_label.configure(text=u"ошибка")
        else:
            self.t_valid_label.configure(text=u"")

        if not (is_valid_b and is_valid_x0 and is_valid_y0 and is_valid_t):
            self.results_label.configure(text=u"Ошибка: проверьте корректность введенных параметров")
            return

        p = self.p_entry.get()
        s = self.s_entry.get()
        f = self.f_entry.get()
        z = self.z_entry.get()
        try:
            bad_string, c1, c2 = compute_manual(p_str=p, s_str=s, f_str=f, z_str=z, b=b, x0=x0, y0=y0, T=t)
            if 'p' in bad_string:
                self.p_valid_label.configure(text=u"ошибка")
            else:
                self.p_valid_label.configure(text=u"")
            if 's' in bad_string:
                self.s_valid_label.configure(text=u"ошибка")
            else:
                self.s_valid_label.configure(text=u"")
            if 'z' in bad_string:
                self.z_valid_label.configure(text=u"ошибка")
            else:
                self.z_valid_label.configure(text=u"")
            if 'f' in bad_string:
                self.f_valid_label.configure(text=u"ошибка")
            else:
                self.f_valid_label.configure(text=u"")
            
            if bad_string == '':
                print (c1, c2)
                criteria = u"Критерий точности попадания в таргет С1 = " + str(c1) + u"\nКритерий открута С2 = " + str(c2)
                self.results_label.configure(text=criteria)
            else:
                self.results_label.configure(text=u"Ошибка: проверьте корректность введенных функций")
                return
        except:
            self.results_label.configure(text=u"Ошибка: проверьте корректность введенных функций")


class AutoMode(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.title_label = ttk.Label(self, text=u"Автоматический режим", font=TITLEFONT, 
                                    background=BGCOLOR, foreground=FGCOLOR)
        self.title_label.grid(row=0, column=0, columnspan=7, pady=20, padx=0)

        self.info_title_label = ttk.Label(self, text=u"Формат ввода", font=LARGEFONT,
                                    width=30,
                                    background=BGCOLOR, foreground=FGCOLOR)
        self.info_title_label.grid(row=2, column=6, pady=0, padx=50)

        self.info_text = tk.Text(self, font=SMALLFONT,
                                    width=50, relief='flat',
                                    background=BGCOLOR, foreground=FGCOLOR)
        self.info_text.grid(row=3, rowspan=10, column=6, pady=0, padx=0)
        self.info_text.insert(tk.INSERT, u"В вещественных числах целая и вещественная часть разделяются точкой.\nПараметры должны быть обозначены заглавными \nлатинскими буквами.\nОперация умножения задается знаком *,\nделения — /, возведения в степень — **.\nВозможно использование математических констант e, pi и функций sin, cos, tan, exp, log. \n\nФункции могут быть заданы неявно: в поле ввода \nукажите полный путь к файлу, где они хранятся в \nтабулированном виде в формате \nN N точек N значений, N>3")
        self.info_text.configure(state="disabled")

        self.show_example1_button = tk.Button(self, text=u"Показать пример №1", font=FONT,
                                        background=BGCOLOR, foreground=FGCOLOR, relief="flat",
                                        activebackground=ACTIVEBGCOLOR, activeforeground=ACTIVEFGCOLOR,
                                        command=self.show_example1)
        self.show_example1_button.grid(row=10, column=6, padx=0, pady=0)

        self.show_example2_button = tk.Button(self, text=u"Показать пример №2", font=FONT,
                                        background=BGCOLOR, foreground=FGCOLOR, relief="flat",
                                        activebackground=ACTIVEBGCOLOR, activeforeground=ACTIVEFGCOLOR,
                                        command=self.show_example2)
        self.show_example2_button.grid(row=11, column=6, padx=0, pady=0)

        self.show_example3_button = tk.Button(self, text=u"Показать пример №3", font=FONT,
                                        background=BGCOLOR, foreground=FGCOLOR, relief="flat",
                                        activebackground=ACTIVEBGCOLOR, activeforeground=ACTIVEFGCOLOR,
                                        command=self.show_example3)
        self.show_example3_button.grid(row=12, column=6, padx=0, pady=0)

        
        self.p_label = ttk.Label(self, text=u"Плотность распределения p(w)", font=FONT, 
                                anchor='e', width=30, 
                                background=BGCOLOR, foreground=FGCOLOR)
        self.p_label.grid(row=1, column=0, padx=0, pady=0)
        self.p_equation_label = ttk.Label(self, text=u" = ", font=FONT,
                                    anchor='w', width=4, 
                                    background=BGCOLOR, foreground=FGCOLOR)
        self.p_equation_label.grid(row=1, column=1, padx=0, pady=0)
        self.p_entry = tk.Entry(self, width=30)
        self.p_entry.grid(row=1, column=2, columnspan=4, padx=0, pady=0, sticky='w')
        self.p_valid_label = ttk.Label(self, text=u"", font=VALIDFONT, 
                                        anchor='w', width=30,
                                        background=BGCOLOR, foreground=VALIDCOLOR)
        self.p_valid_label.grid(row=2, column=2, columnspan=4, padx=0, pady=0)

        
        self.s_label = ttk.Label(self, text=u"План открута S(t)", font=FONT, 
                                anchor='e', width=30, 
                                background=BGCOLOR, foreground=FGCOLOR)
        self.s_label.grid(row=3, column=0, padx=0, pady=0)
        self.s_equation_label = ttk.Label(self, text=u" = ", font=FONT,
                                    anchor='w', width=4, 
                                    background=BGCOLOR, foreground=FGCOLOR)
        self.s_equation_label.grid(row=3, column=1, padx=0, pady=0)
        self.s_entry = tk.Entry(self, width=30)
        self.s_entry.grid(row=3, column=2, columnspan=4, padx=0, pady=0, sticky='w')
        self.s_valid_label = ttk.Label(self, text=u"", font=VALIDFONT, 
                                        anchor='w', width=30,
                                        background=BGCOLOR, foreground=VALIDCOLOR)
        self.s_valid_label.grid(row=4, column=2, columnspan=4, padx=0, pady=0)

        
        self.f_label = ttk.Label(self, text=u"Функция коррекции f(z, X, S, b)", font=FONT,
                                anchor='e', width=30, 
                                background=BGCOLOR, foreground=FGCOLOR)
        self.f_label.grid(row=5, column=0, padx=0, pady=0)
        self.f_equation_label = ttk.Label(self, text=u" = ", font=FONT,
                                    anchor='w', width=4, 
                                    background=BGCOLOR, foreground=FGCOLOR)
        self.f_equation_label.grid(row=5, column=1, padx=0, pady=0)
        self.f_entry = tk.Entry(self, width=30)
        self.f_entry.grid(row=5, column=2, columnspan=4, padx=0, pady=0, sticky='w')
        self.f_valid_label = ttk.Label(self, text=u"", font=VALIDFONT, 
                                        anchor='w', width=30,
                                        background=BGCOLOR, foreground=VALIDCOLOR)
        self.f_valid_label.grid(row=6, column=2, columnspan=4, padx=0, pady=0)

        
        self.b_label = ttk.Label(self, text=u"b ", font=FONT,
                                anchor='e', width=30, 
                                background=BGCOLOR, foreground=FGCOLOR)
        self.b_label.grid(row=7, column=0, padx=0, pady=0)
        self.b_left_parenthesis_label = ttk.Label(self, text=u" = [", font=FONT,
                                                anchor='w', width=4, 
                                                background=BGCOLOR, foreground=FGCOLOR)
        self.b_left_parenthesis_label.grid(row=7, column=1, padx=0, pady=0)
        self.b_from_entry = tk.Entry(self, width=10)
        self.b_from_entry.grid(row=7, column=2, padx=0, pady=0)
        self.b_from_valid_label = ttk.Label(self, text=u"", font=VALIDFONT, 
                                            anchor='w', width=10,
                                            background=BGCOLOR, foreground=VALIDCOLOR)
        self.b_from_valid_label.grid(row=8, column=2, padx=0, pady=0)
        self.b_comma_label = ttk.Label(self, text=u", ", font=FONT,
                                        anchor='w', width=4, 
                                        background=BGCOLOR, foreground=FGCOLOR)
        self.b_comma_label.grid(row=7, column=3, padx=0, pady=0)
        self.b_to_entry = tk.Entry(self, width=10)
        self.b_to_entry.grid(row=7, column=4, padx=0, pady=0)
        self.b_to_valid_label = ttk.Label(self, text=u"", font=VALIDFONT, 
                                        anchor='w', width=10,
                                        background=BGCOLOR, foreground=VALIDCOLOR)
        self.b_to_valid_label.grid(row=8, column=4, padx=0, pady=0)
        self.b_right_parenthesis_label = ttk.Label(self, text=u"]", font=FONT,
                                                    anchor='w', width=2, 
                                                    background=BGCOLOR, foreground=FGCOLOR)
        self.b_right_parenthesis_label.grid(row=7, column=5, padx=0, pady=0)


        self.z_label = ttk.Label(self, text=u"Функция трафика z(t)", font=FONT,
                                anchor='e', width=30, 
                                background=BGCOLOR, foreground=FGCOLOR)
        self.z_label.grid(row=9, column=0, padx=0, pady=0)
        self.z_equation_label = ttk.Label(self, text=u" = ", font=FONT,
                                    anchor='w', width=4, 
                                    background=BGCOLOR, foreground=FGCOLOR)
        self.z_equation_label.grid(row=9, column=1, padx=0, pady=0)
        self.z_entry = tk.Entry(self, width=30)
        self.z_entry.grid(row=9, column=2, columnspan=4, padx=0, pady=0, sticky='w')
        self.z_valid_label = ttk.Label(self, text=u"", font=VALIDFONT, 
                                        anchor='w', width=30,
                                        background=BGCOLOR, foreground=VALIDCOLOR)
        self.z_valid_label.grid(row=10, column=2, columnspan=4, padx=0, pady=0)

        
        self.xy_label = ttk.Label(self, text=u"Начальные условия: X0", font=FONT,
                                anchor='e', width=30, 
                                background=BGCOLOR, foreground=FGCOLOR)
        self.xy_label.grid(row=11, column=0, padx=0, pady=0)

        self.x0_equation_label = ttk.Label(self, text=u" = ", font=FONT,
                                    anchor='w', width=4, 
                                    background=BGCOLOR, foreground=FGCOLOR)
        self.x0_equation_label.grid(row=11, column=1, padx=0, pady=0)
        self.x0_default_value = ttk.Label(self, text=u"S(0)", font=FONT,
                                anchor='w', width=4, 
                                background=BGCOLOR, foreground=FGCOLOR)
        self.x0_default_value.grid(row=11, column=2, padx=0, pady=0)
        
        self.y0_label = ttk.Label(self, text=u"Y0 =", font=FONT,
                                anchor='e', width=4, 
                                background=BGCOLOR, foreground=FGCOLOR)
        self.y0_label.grid(row=11, column=3, padx=0, pady=0)
        self.y0_entry = tk.Entry(self, width=10)
        self.y0_entry.grid(row=11, column=4, padx=0, pady=0)
        self.y0_valid_label = ttk.Label(self, text=u"", font=VALIDFONT, 
                                        anchor='w', width=10,
                                        background=BGCOLOR, foreground=VALIDCOLOR)
        self.y0_valid_label.grid(row=12, column=4, padx=0, pady=0)

        self.t_label = ttk.Label(self, text=u"T_final", font=FONT,
                                anchor='e', width=30, 
                                background=BGCOLOR, foreground=FGCOLOR)
        self.t_label.grid(row=13, column=0, padx=0, pady=0)
        self.t_equation_label = ttk.Label(self, text=u" = ", font=FONT,
                                    anchor='w', width=4, 
                                    background=BGCOLOR, foreground=FGCOLOR)
        self.t_equation_label.grid(row=13, column=1, padx=0, pady=0)
        self.t_entry = tk.Entry(self, width=30)
        self.t_entry.grid(row=13, column=2, columnspan=4, padx=0, pady=0, sticky='w')
        self.t_valid_label = ttk.Label(self, text=u"", font=VALIDFONT, 
                                        anchor='w', width=30,
                                        background=BGCOLOR, foreground=VALIDCOLOR)
        self.t_valid_label.grid(row=14, column=2, columnspan=4, padx=0, pady=0)   


        self.compute_button = tk.Button(self, text=u"Рассчитать", font=LARGEFONT,
                                        background=BGCOLOR, foreground=FGCOLOR, relief="flat",
                                        activebackground=ACTIVEBGCOLOR, activeforeground=ACTIVEFGCOLOR,
                                        command=self.compute)
        self.compute_button.grid(row=15, column=0, columnspan=7, padx=0, pady=10)

        self.results_label = ttk.Label(self, text=u"", font=FONT, 
                                        background=BGCOLOR, foreground=FGCOLOR)
        self.results_label.grid(row=16, column=0, columnspan=7, pady=5)

        self.return_button = tk.Button(self, text=u"Вернуться на главную", font=FONT,
                                        background=BGCOLOR, foreground=FGCOLOR, relief="flat",
                                        activebackground=ACTIVEBGCOLOR, activeforeground=ACTIVEFGCOLOR,
                                        command=lambda: controller.show_frame(StartPage))
        self.return_button.grid(row=17, column=0, columnspan=7)

        self.p_entry.insert(0, "6*W*(1-W)")
        self.s_entry.insert(0, "3*T + sin(T)")
        self.f_entry.insert(0, "B*(S-X)")
        self.z_entry.insert(0, "4*T + cos(T)")
        self.b_from_entry.insert(0, "0.01")
        self.b_to_entry.insert(0, "0.02")
        self.y0_entry.insert(0, "0.0")
        self.t_entry.insert(0, "1")


    def delete_entries(self):
        self.p_entry.delete(0, END)
        self.s_entry.delete(0, END)
        self.f_entry.delete(0, END)
        self.z_entry.delete(0, END)
        self.b_from_entry.delete(0, END)
        self.b_to_entry.delete(0, END)
        self.y0_entry.delete(0, END)
        self.t_entry.delete(0, END)


    def show_example1(self):
        self.delete_entries()
        self.p_entry.insert(0, "p.txt")
        self.s_entry.insert(0, "s.txt")
        self.f_entry.insert(0, "B*(S-X)")
        self.z_entry.insert(0, "z.txt")
        self.b_from_entry.insert(0, "0")
        self.b_to_entry.insert(0, "0.1")
        self.y0_entry.insert(0, "0.0")
        self.t_entry.insert(0, "1")


    def show_example2(self):
        self.delete_entries()
        self.p_entry.insert(0, "1")
        self.s_entry.insert(0, "T")
        self.f_entry.insert(0, "B*(S-X)")
        self.z_entry.insert(0, "4*T")
        self.b_from_entry.insert(0, "0")
        self.b_to_entry.insert(0, "1")
        self.y0_entry.insert(0, "0.0")
        self.t_entry.insert(0, "1")


    def show_example3(self):
        self.delete_entries()
        self.p_entry.insert(0, "2*W")
        self.s_entry.insert(0, "10*T*T")
        self.f_entry.insert(0, "1")
        self.z_entry.insert(0, "T*T")
        self.b_from_entry.insert(0, "0")
        self.b_to_entry.insert(0, "0")
        self.y0_entry.insert(0, "0.0")
        self.t_entry.insert(0, "1")



    def compute(self):
        print("compute called")

        self.p_valid_label.configure(text=u"")
        self.s_valid_label.configure(text=u"")
        self.z_valid_label.configure(text=u"")
        self.f_valid_label.configure(text=u"")
        self.b_from_valid_label.configure(text=u"")
        self.b_to_valid_label.configure(text=u"")
        self.y0_valid_label.configure(text=u"")
        self.t_valid_label.configure(text=u"")
        self.results_label.configure(text=u"")
        
        is_valid_b_from, b_from = validate_float(self.b_from_entry.get())
        if not is_valid_b_from:
            self.b_from_valid_label.configure(text=u"ошибка")
        else:
            self.b_from_valid_label.configure(text=u"")

        is_valid_b_to, b_to = validate_float(self.b_to_entry.get())
        if is_valid_b_to and is_valid_b_from and b_to < b_from:
            is_valid_b_to = False
        if not is_valid_b_to:
            self.b_to_valid_label.configure(text=u"ошибка")
        else:
            self.b_to_valid_label.configure(text=u"")
        
        is_valid_y0, y0 = validate_float(self.y0_entry.get())
        if is_valid_y0 and y0>1:
            is_valid_y0 = False
        if not is_valid_y0:
            self.y0_valid_label.configure(text=u"ошибка")
        else:
            self.y0_valid_label.configure(text=u"")

        is_valid_t, t = validate_float(self.t_entry.get())
        if not is_valid_t:
            self.t_valid_label.configure(text=u"ошибка")
        else:
            self.t_valid_label.configure(text=u"")

        if not (is_valid_b_from and is_valid_b_to and is_valid_y0 and is_valid_t):
            self.results_label.configure(text=u"Ошибка: проверьте корректность введенных параметров")
            return

        p = self.p_entry.get()
        s = self.s_entry.get()
        f = self.f_entry.get()
        z = self.z_entry.get()
        try:
            bad_string, c1, c2 = compute_auto(p_str=p, s_str=s, f_str=f, z_str=z, b_from=b_from, b_to=b_to, y0=y0, T=t)
            if 'p' in bad_string:
                self.p_valid_label.configure(text=u"ошибка")
            else:
                self.p_valid_label.configure(text=u"")
            if 's' in bad_string:
                self.s_valid_label.configure(text=u"ошибка")
            else:
                self.s_valid_label.configure(text=u"")
            if 'z' in bad_string:
                self.z_valid_label.configure(text=u"ошибка")
            else:
                self.z_valid_label.configure(text=u"")
            if 'f' in bad_string:
                self.f_valid_label.configure(text=u"ошибка")
            else:
                self.f_valid_label.configure(text=u"")
            
            if bad_string == '':
                print (c1, c2)
                criteria = u"Критерий точности попадания в таргет С1 = " + str(c1) + u"\nКритерий открута С2 = " + str(c2)
                self.results_label.configure(text=criteria)
            else:
                self.results_label.configure(text=u"Ошибка: проверьте корректность введенных функций")
                return
        except:
            self.results_label.configure(text=u"Ошибка: проверьте корректность введенных функций")


        
app = Demo()
app.mainloop()