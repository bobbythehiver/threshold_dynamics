# -*- coding: utf-8 -*-

import numpy as np
from numpy import e, pi, exp, log, sin, cos, tan
import scipy as sc
from scipy.linalg import det, inv
from scipy.integrate import quad
from scipy.integrate import odeint
import matplotlib.pyplot as plt
from matplotlib import rc

from solve import compute_manual, compute_auto, create_filename
from cauchy import solve_cauchy, solve_linear
from interpolation import tabulate, get_points
from interpolation import interpolate, interpolate_tab, recover
from integration import integrate

from funcio import read_splines_from_file, write_splines_to_file
from funcio import read_func_from_str, read_f
from funcio import read_tabulated_func_from_file, write_tabulated_func_to_file

 
font = {'family': 'Verdana',
        'weight': 'normal',
        'size': 12}
rc('font', **font)

UNI=0
CHEB=1

def draw_interpolation_curves(f, a, b, title):
	n = 10
	h = (b - a) / float(n)
	xn = get_points(a, b, n, mode=UNI)
	fn = tabulate(f, xn)
	f_interpolated = recover(interpolate(f, xn))

	x = get_points(a, b, 100, mode=UNI)
	y_interpolated = tabulate(f_interpolated, x)
	y = tabulate(f, x)

	plt.figure()
	plt.subplot(211)
	points, = plt.plot(xn, fn, 'ro') 
	f_interpolated_line, = plt.plot(x, y_interpolated, 'b')
	f_line, = plt.plot(x, y, 'b--')
	plt.legend([points, f_interpolated_line, f_line], 
				[u"опорные точки", u"результат интерполяции", u"исходная функция"], 
				fontsize=8, loc="best")
	plt.xlabel('x')
	plt.ylabel('y')
	plt.title(title + u'\nh = %.2f' % h)
	plt.grid()

	n = 20
	h = (b - a) / float(n)
	xn = get_points(a, b, n, mode=UNI)
	fn = tabulate(f, xn)
	f_interpolated = recover(interpolate(f, xn))

	x = get_points(a, b, 100, mode=UNI)
	y_interpolated = tabulate(f_interpolated, x)
	y = tabulate(f, x)
	error = [y_interpolated[i] - y[i] for i in range(len(x))]

	plt.subplot(212)
	plt.title(title)
	points, = plt.plot(xn, fn, 'ro') 
	f_interpolated_line, = plt.plot(x, y_interpolated, 'b')
	f_line, = plt.plot(x, y, 'b--')
	plt.legend([points, f_interpolated_line, f_line], 
				[u"опорные точки", u"результат интерполяции", u"исходная функция"], 
				fontsize=8, loc="best")
	plt.xlabel('x')
	plt.ylabel('y')
	plt.title(u'h = %.2f' % h)
	plt.tight_layout()
	plt.grid()
	
	plt.figure()
	plt.title(u"погрешность интерполяции, h = %.2f" % h)
	error_line, = plt.plot(x, error, 'r')
	plt.xlabel('x')
	plt.ylabel('error')
	plt.grid()
	plt.tight_layout()
	
	plt.show()


def visualize_interpolation_cases():
	a = 0
	b = 10
	of = lambda x: np.sin(x)
	draw_interpolation_curves(of, a, b, u"Интерполяция. sin(x) на [%d, %d]" % (a, b))
	
	sf = lambda x: np.log(np.fabs(x+1))
	draw_interpolation_curves(sf, a, b, u"Интерполяция. log(x + 1) на [%d, %d]" % (a, b))

	df = lambda x: np.floor(x) % 2
	draw_interpolation_curves(df, a, b, u"Интерполяция. [x] mod 2 на [%d, %d]" % (a, b))


def draw_integration_curves(f, a, b, title):
	n = np.arange(3, 100, 1)

	f_true_int_value, f_true_int_error = quad(f, a, b)
	f_true_int = [f_true_int_value for i in n]

	f_int = []
	f_err = []
	for i in n:
		xn = get_points(a, b, i+1, mode=UNI)
		f_interpolated = recover(interpolate_tab(xn, integrate(f, xn)))
		f_int_value = f_interpolated(b)
		f_int.append(f_int_value)

	plt.figure()
	#plt.subplot(211)
	plt.title(title)
	f_true_int_line, = plt.plot(n, f_true_int, 'r')
	f_int_line, = plt.plot(n, f_int, 'b')
	plt.legend([f_true_int_line, f_int_line], 
		[u"точное значение", u"формула Симпсона"])
	plt.xlabel(u'n - кол-во интервалов')
	plt.ylabel('y')

	log_n = np.array([log(i) for i in n])
	log_err = np.array([log(abs(diff)) for diff in (np.array(f_int) - np.array(f_true_int))])
	
	X = np.array([log_n, np.ones(len(log_n))]).T
	w = np.linalg.inv(X.T.dot(X)).dot(X.T).dot(log_err)
	plt.figure()
	#plt.subplot(212)
	err_true_line, = plt.plot(log_n, log_err, 'r')
	err_line, = plt.plot(log_n, log_n*w[0] + w[1], 'b')
	plt.legend([err_true_line, err_line], 
		[u"log(error)", u"%.2f*log(n)+(%.2f)"%(w[0], w[1])])
	plt.xlabel('log(n)')
	plt.ylabel('log(error)')
	plt.grid()
	plt.tight_layout()
	
	plt.show()


def visualize_integration_cases():
	a = 0
	b = 10
	of = lambda x: np.sin(x)
	draw_integration_curves(of, a, b, u"Интегрирование. sin(x) на [%d, %d]" % (a, b))
	
	sf = lambda x: np.log(x+1)
	draw_integration_curves(sf, a, b, u"Интегрирование. log(x + 1)  на [%d, %d]" % (a, b))
	
	df = lambda x: np.floor(x) % 2
	draw_integration_curves(df, a, b, u"Интегрирование. [x] mod 2 на [%d, %d]" % (a, b))


def test_compute_manual():
	p = lambda w: 6*w*(1-w)
	wn = get_points(0, 1, 20)
	write_tabulated_func_to_file(wn, tabulate(p, wn), 'p.txt')
	p_str = 'p.txt'
	#p_str = "6*W*(1-W)"
	
	s = lambda t: 3*t + sin(t) 
	tn = get_points(0, 1, 20)
	write_tabulated_func_to_file(tn, tabulate(s, tn), 's.txt')
	s_str = 's.txt'
	#s_str = "3*T + sin(T)"
	
	f_str = "B*(S-X)"

	z = lambda t: 4*t + cos(t) 
	tn = get_points(0, 1, 20)
	write_tabulated_func_to_file(tn, tabulate(z, tn), 'z.txt')
	z_str = 'z.txt'
	#z_str = "4*T + cos(T)"
	
	b = 0.01
	x0 = 0
	y0 = 0
	T = 1
	print compute_manual(p_str, s_str, f_str, z_str, b, x0, y0, T)


def test_compute_auto():
	p_str = "6*W*(1-W)"
	s_str = "3*T + sin(T)"
	f_str = "B*(S-X)"
	z_str = "4*T + cos(T)"
	b_from = 0.01
	b_to = 0.02
	y0 = 0
	T = 1
	print compute_auto(p_str, s_str, f_str, z_str, b_from, b_to, y0, T)


def test_linear():
	error = lambda b, b_pred: sum(np.fabs(b - b_pred))
	norm = lambda A: max(np.fabs(A).reshape(A.shape[0]*A.shape[1], ))
	mu = lambda A: norm(A) * norm(inv(A))
	stat = lambda A: (det(A), norm(A), mu(A))

	A = np.array([[1, 2, 3], [2.0001,3.999, 6], [15, 3, 6]])
	b = np.array([1, 0, 0])
	x = solve_linear(A, b)
	print stat(A)
	print error(b, A.dot(x))

	B = np.zeros((8, 8))
	for i in range(B.shape[0]):
		for j in range(B.shape[1]):
			B[i][j] = 1.0 / (i+j+1) 
	b = np.array([1, 0, 0, 0, 0, 0, 0, 0])
	x = solve_linear(B, b)
	print stat(B)
	print error(b, B.dot(x))

	C = np.array([[10**6, 2], [10**13, 2]])
	b = np.array([1, 0])
	x = solve_linear(C, b)
	print stat(C)
	print error(b, C.dot(x))


def test_cauchy():
	x0 = 6
	y0 = 5
	tn = np.arange(0.0, 1.0, 0.01)
	g = lambda t, x, y: 2*x - 5*y + 3
	f = lambda t, x, y: 5*x - 6*y + 1

	xn, yn = solve_cauchy(x0=x0, y0=y0, tn=tn, g=g, f=f)

	x_filename = create_filename('x')
	x_splines = interpolate_tab(tn, xn)
	write_splines_to_file(x_splines, x_filename)
	#x = convert_splines_to_func(read_splines_from_file(x_filename))

	y_filename = create_filename('y')
	y_splines = interpolate_tab(tn, yn)
	write_splines_to_file(y_splines, y_filename)
	#y = convert_splines_to_func(read_splines_from_file(y_filename))

	sol = np.array(zip(*list([tabulate(lambda t: exp(-2*t)*5*cos(3*t) + 1, tn), 
		tabulate(lambda t: exp(-2*t)*(4*cos(3*t) + 3*sin(t)) + 1, tn)])))

	plt.figure()
	plt.subplot(221)
	xline, = plt.plot(tn, xn, 'b')
	true_xline, = plt.plot(tn, sol[:, 0], 'r')
	plt.legend([xline, true_xline], [u'x по методу Рунге-Кутты', u'точное x'], 
		fontsize=10, loc="best")
	plt.xlabel('t')
	plt.ylabel('x')
	plt.title(u'x(t)')
	plt.grid()

	plt.subplot(222)
	yline, = plt.plot(tn, yn, 'b')
	true_yline, = plt.plot(tn, sol[:, 1], 'r')
	plt.legend([yline, true_yline], [u'y по методу Рунге-Кутты', u'точное y'], 
		fontsize=10, loc="best")
	plt.xlabel('t')
	plt.ylabel('y')
	plt.title(u'y(t)')
	plt.grid()

	log_tn = [log(t) for t in tn]
	log_err = [log(abs(diff)) for diff in (np.array(xn) - np.array(sol[:, 0]))]
	plt.subplot(223)
	plt.plot(log_tn, log_err, 'r')
	plt.xlabel('log(t)')
	plt.ylabel('log(error)')
	plt.title(u'погрешность решения: x(t)')
	plt.grid()

	log_err = [log(abs(diff)) for diff in (np.array(yn) - np.array(sol[:, 1]))]
	plt.subplot(224)
	plt.plot(log_tn, log_err, 'r')
	plt.xlabel('log(t)')
	plt.ylabel('log(error)')
	plt.title(u'погрешность решения: y(t)')
	plt.grid()
	plt.tight_layout()

	plt.figure()
	true_xyline, = plt.plot(sol[:, 0], sol[:, 1], 'r')
	xyline, = plt.plot(xn, yn, 'b')
	plt.legend([xyline, true_xyline], [u'по методу Рунге-Кутты', u'точная'],
		loc="best")
	plt.xlabel('x')
	plt.ylabel('y')
	plt.title(u'Траектория решений задачи Коши')
	plt.grid()
	plt.tight_layout()

	X, Y = np.meshgrid(np.arange(-10, 10, 0.01), np.arange(-10, 10, 0.01))
	#X, Y = np.meshgrid(xn, yn)
	G = g(t, X, Y)
	F = f(t, X, Y)
	plt.figure()
	plt.streamplot(X, Y, G, F, density=[1, 1], color="k")
	plt.title(u"Векторное поле")
	plt.show()


def tabulate_write():
	p = lambda w: 6*w*(1-w)
	wn = get_points(0, 1, 100, mode=UNI)
	write_tabulated_func_to_file(wn, tabulate(p, wn), 'p.txt')
	p_str = 'p.txt'
	#p_str = "6*W*(1-W)"
	
	s = lambda t: 3*t + sin(t) 
	tn = get_points(0, 1, 100, mode=UNI)
	write_tabulated_func_to_file(tn, tabulate(s, tn), 's.txt')
	s_str = 's.txt'
	#s_str = "3*T + sin(T)"
	
	f_str = "B*(X-Z)"

	z = lambda t: 4*t + cos(t) 
	tn = get_points(0, 1, 100, mode=UNI)
	write_tabulated_func_to_file(tn, tabulate(z, tn), 'z.txt')
	z_str = 'z.txt'
	#z_str = "4*T + cos(T)"


def read_integrate_interpolate_write():
	file_p, p = read_tabulated_func_from_file('p.txt')
	a = 0
	b = 1
	yn = get_points(a, b, 100, mode=UNI)
	u_filename = create_filename('u')
	u_splines = interpolate(yn, integrate(p, yn))
	write_splines_to_file(u_splines, u_filename)
	u = recover(u_splines)
	#u = recover(read_splines_from_file(u_filename))

	yn = get_points(a, b, 1000, mode=UNI)
	un = tabulate(u, yn)
	un_true = [quad(p, a, y)[0] for y in yn]

	plt.figure()
	plt.subplot(211)
	u_true_line, = plt.plot(yn, un_true, 'r')
	u_line, = plt.plot(yn, un, 'b')
	plt.legend([u_true_line, u_line], 
		[u"действительное значение интеграла", u"результат численного интегрирования"])
	plt.xlabel('y')
	plt.ylabel('p')
	plt.grid()

	plt.subplot(212)
	error_line =  plt.plot(yn, np.array(un) - np.array(un_true), 'r')
	plt.xlabel('y')
	plt.ylabel('error')
	plt.grid()
	plt.show()


import sys

modes = ('manual', 'auto', 'interp', 'integr', 'cauchy', 'tab-write', 'int-int', 'linear')

if __name__ == "__main__":
	test_case = sys.argv[1]
	if test_case=='manual':
		test_compute_manual()
	elif test_case=='auto':
		test_compute_auto()
	elif test_case=='interp':
		visualize_interpolation_cases()
	elif test_case=='integr':
		visualize_integration_cases()
	elif test_case=='cauchy':
		test_cauchy()
	elif test_case=='tab-write':
		tabulate_write()
	elif test_case=='int-int':
		read_integrate_interpolate_write()
	elif test_case=='linear':
		test_linear()
	else:
		print "Error: argument not in", modes


